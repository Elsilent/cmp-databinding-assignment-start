import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  counterArray = [];
  onCounterChanged (counterObj: {counter: number}) {
    this.counterArray.push(counterObj.counter);
  }
}
