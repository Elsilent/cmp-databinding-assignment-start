import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import Timer = NodeJS.Timer;

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  intervalCounter: Timer;
  counter = 0;
  @Output() counterIncreased = new EventEmitter<{counter: number}>();

  constructor() { }

  ngOnInit() {
  }

  onGameStarted() {
    this.intervalCounter = setInterval(() => {
      this.counterIncreased.emit({counter: ++this.counter});
    }, 1000);
  }

  onGameStopped() {
    clearInterval(this.intervalCounter);
  }
}
